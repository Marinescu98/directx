﻿using SharpDX;
using System;

namespace ProjectDirect3D.Common
{
    public static class MathHelper
    {
        public static float Sinf(double a) => (float)Math.Sin(a);
        public static float Cosf(double d) => (float)Math.Cos(d);
        public static float Tanf(double a) => (float)Math.Tan(a);


        /// <summary>
        /// Builds a matrix that can be used to reflect vectors about a plane.
        /// </summary>
        /// <param name="plane">The plane for which the reflection occurs. This parameter is assumed to be normalized.</param>
        /// <param name="result">When the method completes, contains the reflection matrix.</param>
        public static void Reflection(ref Plane plane, out Matrix result)
        {
            float num1 = plane.Normal.X;
            float num2 = plane.Normal.Y;
            float num3 = plane.Normal.Z;
            float num4 = -2f * num1;
            float num5 = -2f * num2;
            float num6 = -2f * num3;
            result.M11 = (float)((double)num4 * (double)num1 + 1.0);
            result.M12 = num5 * num1;
            result.M13 = num6 * num1;
            result.M14 = 0.0f;
            result.M21 = num4 * num2;
            result.M22 = (float)((double)num5 * (double)num2 + 1.0);
            result.M23 = num6 * num2;
            result.M24 = 0.0f;
            result.M31 = num4 * num3;
            result.M32 = num5 * num3;
            result.M33 = (float)((double)num6 * (double)num3 + 1.0);
            result.M34 = 0.0f;
            result.M41 = num4 * plane.D;
            result.M42 = num5 * plane.D;
            result.M43 = num6 * plane.D;
            result.M44 = 1f;
        }

        /// <summary>
        /// Creates a matrix that flattens geometry into a shadow.
        /// </summary>
        /// <param name="light">The light direction. If the W component is 0, the light is directional light; if the
        /// W component is 1, the light is a point light.</param>
        /// <param name="plane">The plane onto which to project the geometry as a shadow. This parameter is assumed to be normalized.</param>
        /// <param name="result">When the method completes, contains the shadow matrix.</param>
        public static void Shadow(ref Vector4 light, ref Plane plane, out Matrix result)
        {
            float num1 = (float)((double)plane.Normal.X * (double)light.X + (double)plane.Normal.Y * (double)light.Y + (double)plane.Normal.Z * (double)light.Z + (double)plane.D * (double)light.W);
            float num2 = -plane.Normal.X;
            float num3 = -plane.Normal.Y;
            float num4 = -plane.Normal.Z;
            float num5 = -plane.D;
            result.M11 = num2 * light.X + num1;
            result.M21 = num3 * light.X;
            result.M31 = num4 * light.X;
            result.M41 = num5 * light.X;
            result.M12 = num2 * light.Y;
            result.M22 = num3 * light.Y + num1;
            result.M32 = num4 * light.Y;
            result.M42 = num5 * light.Y;
            result.M13 = num2 * light.Z;
            result.M23 = num3 * light.Z;
            result.M33 = num4 * light.Z + num1;
            result.M43 = num5 * light.Z;
            result.M14 = num2 * light.W;
            result.M24 = num3 * light.W;
            result.M34 = num4 * light.W;
            result.M44 = num5 * light.W + num1;
        }
    }
}
