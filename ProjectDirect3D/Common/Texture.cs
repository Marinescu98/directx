﻿using SharpDX.Direct3D12;
using System;
namespace ProjectDirect3D.Common
{
    public class Texture : IDisposable
    {
        // Unique material name for lookup.
        public string Name { get; set; }

        public string Filename { get; set; }

        public Resource Resource { get; set; }
        public Resource UploadHeap { get; set; }

        public void Dispose()
        {
            Resource?.Dispose();
            UploadHeap?.Dispose();
        }
    }
}
