﻿using SharpDX;

namespace ProjectDirect3D.Common
{
    // Simple struct to represent a material for our demos. A production 3D engine
    // would likely create a class hierarchy of Materials.
    public class Material
    {
        // Unique material name for lookup.
        public string Name { get; set; }

        // Index into constant buffer corresponding to this material.
        public int MatCBIndex { get; set; } = -1;

        // Index into SRV heap for diffuse texture.
        public int DiffuseSrvHeapIndex { get; set; } = -1;

        // Dirty flag indicating the material has changed and we need to update the constant buffer.
        // Because we have a material constant buffer for each FrameResource, we have to apply the
        // update to each FrameResource. Thus, when we modify a material we should set
        // NumFramesDirty = NumFrameResources so that each frame resource gets the update.
        public int NumFramesDirty { get; set; } = D3DApp.NumFrameResources;

        // Material constant buffer data used for shading.
        public Vector4 DiffuseAlbedo { get; set; } = Vector4.One;
        public Vector3 FresnelR0 { get; set; } = new Vector3(0.01f);
        public float Roughness { get; set; } = 0.25f;
        public Matrix MatTransform { get; set; } = Matrix.Identity;
    }

}
