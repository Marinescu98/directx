﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using ProjectDirect3D.Common;
using SharpDX;
using SharpDX.Direct3D;
using SharpDX.Direct3D12;
using SharpDX.DXGI;
using Resource = SharpDX.Direct3D12.Resource;
using ShaderResourceViewDimension = SharpDX.Direct3D12.ShaderResourceViewDimension;

namespace ProjectDirect3D
{
    public class CubeMapApp : D3DApp
    {
        private readonly List<FrameResource> _frameResources = new List<FrameResource>(NumFrameResources);
        private readonly List<AutoResetEvent> _fenceEvents = new List<AutoResetEvent>(NumFrameResources);
        private int _currFrameResourceIndex;

        private RootSignature _rootSignature;

        private DescriptorHeap _srvDescriptorHeap;
        private DescriptorHeap[] _descriptorHeaps;

        private readonly Dictionary<string, MeshGeometry> _geometries = new Dictionary<string, MeshGeometry>();
        private readonly Dictionary<string, Material> _materials = new Dictionary<string, Material>();
        private readonly Dictionary<string, Texture> _textures = new Dictionary<string, Texture>();
        private readonly Dictionary<string, ShaderBytecode> _shaders = new Dictionary<string, ShaderBytecode>();
        private readonly Dictionary<string, PipelineState> _psos = new Dictionary<string, PipelineState>();

        private InputLayoutDescription _inputLayout;

        // List of all the render items.
        private readonly List<RenderItem> _allRitems = new List<RenderItem>();

        // Render items divided by PSO.
        private readonly Dictionary<RenderLayer, List<RenderItem>> _ritemLayers = new Dictionary<RenderLayer, List<RenderItem>>
        {
            [RenderLayer.Opaque] = new List<RenderItem>(),
            [RenderLayer.Transparent] = new List<RenderItem>(),
            [RenderLayer.Sky] = new List<RenderItem>(),
        };

        private int _skyTexHeapIndex;

        private PassConstants _mainPassCB = PassConstants.Default;

        private readonly Camera _camera = new Camera();

        private Point _lastMousePos;

        public CubeMapApp()
        {
            MainWindowCaption = "Cube Map";
        }

        private FrameResource CurrFrameResource => _frameResources[_currFrameResourceIndex];
        private AutoResetEvent CurrentFenceEvent => _fenceEvents[_currFrameResourceIndex];

        public override void Initialize()
        {
            base.Initialize();

            // Reset the command list to prep for initialization commands.
            CommandList.Reset(DirectCmdListAlloc, null);

            _camera.Position = new Vector3(0.0f, 20.0f, 0.0f);

            LoadTextures();
            BuildRootSignature();
            BuildDescriptorHeaps();
            BuildShadersAndInputLayout();
            BuildShapeGeometry();
            BuildMaterials();
            BuildRenderItems();
            BuildFrameResources();
            BuildPSOs();

            // Execute the initialization commands.
            CommandList.Close();
            CommandQueue.ExecuteCommandList(CommandList);

            // Wait until initialization is complete.
            FlushCommandQueue();
        }

        protected override void OnResize()
        {
            base.OnResize();

            // The window resized, so update the aspect ratio and recompute the projection matrix.
            _camera.SetLens(MathUtil.PiOverFour, AspectRatio, 1.0f, 1000.0f);
        }

        protected override void Update(GameTimer gt)
        {
            OnKeyboardInput(gt);

            // Cycle through the circular frame resource array.
            _currFrameResourceIndex = (_currFrameResourceIndex + 1) % NumFrameResources;

            // Has the GPU finished processing the commands of the current frame resource?
            // If not, wait until the GPU has completed commands up to this fence point.
            if (CurrFrameResource.Fence != 0 && Fence.CompletedValue < CurrFrameResource.Fence)
            {
                Fence.SetEventOnCompletion(CurrFrameResource.Fence, CurrentFenceEvent.SafeWaitHandle.DangerousGetHandle());
                CurrentFenceEvent.WaitOne();
            }

            UpdateObjectCBs();
            UpdateMaterialBuffer();
            UpdateMainPassCB(gt);
        }

        protected override void Draw(GameTimer gt)
        {
            CommandAllocator cmdListAlloc = CurrFrameResource.CmdListAlloc;

            // Reuse the memory associated with command recording.
            // We can only reset when the associated command lists have finished execution on the GPU.
            cmdListAlloc.Reset();

            // A command list can be reset after it has been added to the command queue via ExecuteCommandList.
            // Reusing the command list reuses memory.
            CommandList.Reset(cmdListAlloc, _psos["opaque"]);

            CommandList.SetViewport(Viewport);
            CommandList.SetScissorRectangles(ScissorRectangle);

            // Indicate a state transition on the resource usage.
            CommandList.ResourceBarrierTransition(CurrentBackBuffer, ResourceStates.Present, ResourceStates.RenderTarget);

            // Clear the back buffer and depth buffer.
            CommandList.ClearRenderTargetView(CurrentBackBufferView, Color.LightSteelBlue);
            CommandList.ClearDepthStencilView(DepthStencilView, ClearFlags.FlagsDepth | ClearFlags.FlagsStencil, 1.0f, 0);

            // Specify the buffers we are going to render to.
            CommandList.SetRenderTargets(CurrentBackBufferView, DepthStencilView);

            CommandList.SetDescriptorHeaps(_descriptorHeaps.Length, _descriptorHeaps);

            CommandList.SetGraphicsRootSignature(_rootSignature);

            Resource passCB = CurrFrameResource.PassCB.Resource;
            CommandList.SetGraphicsRootConstantBufferView(1, passCB.GPUVirtualAddress);

            // Bind all the materials used in this scene. For structured buffers, we can bypass the heap and
            // set as a root descriptor.
            Resource matBuffer = CurrFrameResource.MaterialBuffer.Resource;
            CommandList.SetGraphicsRootShaderResourceView(2, matBuffer.GPUVirtualAddress);

            // Bind the sky cube map. For our demos, we just use one "world" cube map representing the environment
            // from far away, so all objects will use the same cube map and we only need to set it once per-frame.
            // If we wanted to use "local" cube maps, we would have to change them per-object, or dynamically
            // index into an array of cube maps.
            GpuDescriptorHandle skyTexDescriptor = _srvDescriptorHeap.GPUDescriptorHandleForHeapStart;
            skyTexDescriptor += _skyTexHeapIndex * CbvSrvUavDescriptorSize;
            CommandList.SetGraphicsRootDescriptorTable(3, skyTexDescriptor);

            // Bind all the textures used in this scene. Observe
            // that we only have to specify the first descriptor in the table.
            // The root signature knows how many descriptors are expected in the table.
            CommandList.SetGraphicsRootDescriptorTable(4, _srvDescriptorHeap.GPUDescriptorHandleForHeapStart);

            DrawRenderItems(CommandList, _ritemLayers[RenderLayer.Opaque]);

            CommandList.PipelineState = _psos["sky"];
            DrawRenderItems(CommandList, _ritemLayers[RenderLayer.Sky]);

            CommandList.PipelineState = _psos["transparent"];
            DrawRenderItems(CommandList, _ritemLayers[RenderLayer.Transparent]);

            // Indicate a state transition on the resource usage.
            CommandList.ResourceBarrierTransition(CurrentBackBuffer, ResourceStates.RenderTarget, ResourceStates.Present);

            // Done recording commands.
            CommandList.Close();

            // Add the command list to the queue for execution.
            CommandQueue.ExecuteCommandList(CommandList);

            // Present the buffer to the screen. Presenting will automatically swap the back and front buffers.
            SwapChain.Present(0, PresentFlags.None);

            // Advance the fence value to mark commands up to this fence point.
            CurrFrameResource.Fence = ++CurrentFence;

            // Add an instruction to the command queue to set a new fence point.
            // Because we are on the GPU timeline, the new fence point won't be
            // set until the GPU finishes processing all the commands prior to this Signal().
            CommandQueue.Signal(Fence, CurrentFence);
        }

        protected override void OnMouseDown(MouseButtons button, Point location)
        {
            base.OnMouseDown(button, location);
            _lastMousePos = location;
        }

        protected override void OnMouseMove(MouseButtons button, Point location)
        {
            if ((button & MouseButtons.Left) != 0)
            {
                // Make each pixel correspond to a quarter of a degree.
                float dx = MathUtil.DegreesToRadians(0.25f * (location.X - _lastMousePos.X));
                float dy = MathUtil.DegreesToRadians(0.25f * (location.Y - _lastMousePos.Y));

                _camera.Pitch(dy);
                _camera.RotateY(dx);
            }

            _lastMousePos = location;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _rootSignature.Dispose();
                _srvDescriptorHeap?.Dispose();
                foreach (Texture texture in _textures.Values) texture.Dispose();
                foreach (FrameResource frameResource in _frameResources) frameResource.Dispose();
                foreach (MeshGeometry geometry in _geometries.Values) geometry.Dispose();
                foreach (PipelineState pso in _psos.Values) pso.Dispose();
            }
            base.Dispose(disposing);
        }

        private void OnKeyboardInput(GameTimer gt)
        {
            float dt = gt.DeltaTime;

            if (IsKeyDown(Keys.W))
                _camera.Walk(60.0f * dt);
            if (IsKeyDown(Keys.S))
                _camera.Walk(-50.0f * dt);
            if (IsKeyDown(Keys.A))
                _camera.Strafe(-50.0f * dt);
            if (IsKeyDown(Keys.D))
                _camera.Strafe(50.0f * dt);

            _camera.UpdateViewMatrix();
        }

        private void UpdateObjectCBs()
        {
            foreach (RenderItem e in _allRitems)
            {
                // Only update the cbuffer data if the constants have changed.
                // This needs to be tracked per frame resource.
                if (e.NumFramesDirty > 0)
                {
                    var objConstants = new ObjectConstants
                    {
                        World = Matrix.Transpose(e.World),
                        TexTransform = Matrix.Transpose(e.TexTransform),
                        MaterialIndex = e.Mat.MatCBIndex
                    };
                    CurrFrameResource.ObjectCB.CopyData(e.ObjCBIndex, ref objConstants);

                    // Next FrameResource need to be updated too.
                    e.NumFramesDirty--;
                }
            }
        }

        private void UpdateMaterialBuffer()
        {
            UploadBuffer<MaterialData> currMaterialCB = CurrFrameResource.MaterialBuffer;
            foreach (Material mat in _materials.Values)
            {
                // Only update the cbuffer data if the constants have changed. If the cbuffer
                // data changes, it needs to be updated for each FrameResource.
                if (mat.NumFramesDirty > 0)
                {
                    var matConstants = new MaterialData
                    {
                        DiffuseAlbedo = mat.DiffuseAlbedo,
                        FresnelR0 = mat.FresnelR0,
                        Roughness = mat.Roughness,
                        MatTransform = Matrix.Transpose(mat.MatTransform),
                        DiffuseMapIndex = mat.DiffuseSrvHeapIndex
                    };

                    currMaterialCB.CopyData(mat.MatCBIndex, ref matConstants);

                    // Next FrameResource need to be updated too.
                    mat.NumFramesDirty--;
                }
            }
        }

        private void UpdateMainPassCB(GameTimer gt)
        {
            Matrix view = _camera.View;
            Matrix proj = _camera.Proj;

            Matrix viewProj = view * proj;
            Matrix invView = Matrix.Invert(view);
            Matrix invProj = Matrix.Invert(proj);
            Matrix invViewProj = Matrix.Invert(viewProj);

            _mainPassCB.View = Matrix.Transpose(view);
            _mainPassCB.InvView = Matrix.Transpose(invView);
            _mainPassCB.Proj = Matrix.Transpose(proj);
            _mainPassCB.InvProj = Matrix.Transpose(invProj);
            _mainPassCB.ViewProj = Matrix.Transpose(viewProj);
            _mainPassCB.InvViewProj = Matrix.Transpose(invViewProj);
            _mainPassCB.EyePosW = _camera.Position;
            _mainPassCB.RenderTargetSize = new Vector2(ClientWidth, ClientHeight);
            _mainPassCB.InvRenderTargetSize = 1.0f / _mainPassCB.RenderTargetSize;
            _mainPassCB.NearZ = 1.0f;
            _mainPassCB.FarZ = 1000.0f;
            _mainPassCB.TotalTime = gt.TotalTime;
            _mainPassCB.DeltaTime = gt.DeltaTime;
            _mainPassCB.AmbientLight = new Vector4(0.25f, 0.25f, 0.35f, 1.0f);
            _mainPassCB.Lights[0].Direction = new Vector3(0.57735f, -0.57735f, 0.57735f);
            _mainPassCB.Lights[0].Strength = new Vector3(0.6f);
            _mainPassCB.Lights[1].Direction = new Vector3(-0.57735f, -0.57735f, 0.57735f);
            _mainPassCB.Lights[1].Strength = new Vector3(0.3f);
            _mainPassCB.Lights[2].Direction = new Vector3(0.0f, -0.707f, -0.707f);
            _mainPassCB.Lights[2].Strength = new Vector3(0.15f);

            CurrFrameResource.PassCB.CopyData(0, ref _mainPassCB);
        }

        private void LoadTextures()
        {
            AddTexture("houseTex", "house1.dds");
            AddTexture("houseTex3", "house.dds");
            AddTexture("brickTex", "bricks3.dds");
            AddTexture("houseTex2", "house2.dds");
            AddTexture("houseTex4", "house4.dds");
            AddTexture("houseTex5", "house5.dds");
            AddTexture("streetTex", "way.dds");
            AddTexture("treeArrayTex", "treeArray2.dds");
            AddTexture("streetTex1", "street1.dds");
            AddTexture("pineTex", "pine.dds");
            AddTexture("treeTex", "tree02S.dds");
            AddTexture("tree2Tex", "tree35S.dds");
            AddTexture("tree3Tex", "treeArray.dds");
            AddTexture("grassTex", "grass.dds");
            AddTexture("parking", "parking.dds");
            AddTexture("parking2", "parking2.dds");
            AddTexture("skyCubeMap", "grasscube1024.dds");
        }

        private void AddTexture(string name, string filename)
        {
            var tex = new Texture
            {
                Name = name,
                Filename = $"Textures\\{filename}"
            };
            tex.Resource = TextureUtilities.CreateTextureFromDDS(Device, tex.Filename);
            _textures[tex.Name] = tex;
        }

        private void BuildRootSignature()
        {
            // Root parameter can be a table, root descriptor or root constants.
            // Perfomance TIP: Order from most frequent to least frequent.
            var slotRootParameters = new[]
            {
                new RootParameter(ShaderVisibility.All, new RootDescriptor(0, 0), RootParameterType.ConstantBufferView),
                new RootParameter(ShaderVisibility.All, new RootDescriptor(1, 0), RootParameterType.ConstantBufferView),
                new RootParameter(ShaderVisibility.All, new RootDescriptor(0, 1), RootParameterType.ShaderResourceView),
                new RootParameter(ShaderVisibility.All, new DescriptorRange(DescriptorRangeType.ShaderResourceView, 1, 0)),
                new RootParameter(ShaderVisibility.All, new DescriptorRange(DescriptorRangeType.ShaderResourceView, 5, 1))
            };

            // A root signature is an array of root parameters.
            var rootSigDesc = new RootSignatureDescription(
                RootSignatureFlags.AllowInputAssemblerInputLayout,
                slotRootParameters,
                GetStaticSamplers());

            _rootSignature = Device.CreateRootSignature(rootSigDesc.Serialize());
        }

        private void BuildDescriptorHeaps()
        {
            //
            // Create the SRV heap.
            //
            var srvHeapDesc = new DescriptorHeapDescription
            {
                DescriptorCount = _textures.Count,
                Type = DescriptorHeapType.ConstantBufferViewShaderResourceViewUnorderedAccessView,
                Flags = DescriptorHeapFlags.ShaderVisible
            };
            _srvDescriptorHeap = Device.CreateDescriptorHeap(srvHeapDesc);
            _descriptorHeaps = new[] { _srvDescriptorHeap };

            //
            // Fill out the heap with actual descriptors.
            //
            CpuDescriptorHandle hDescriptor = _srvDescriptorHeap.CPUDescriptorHandleForHeapStart;

            Resource[] tex2DList =
            {
                _textures["houseTex"].Resource,
                _textures["houseTex3"].Resource,
                _textures["brickTex"].Resource,
                _textures["houseTex2"].Resource,
                _textures["houseTex4"].Resource,
                _textures["houseTex5"].Resource,
                _textures["streetTex"].Resource,
                _textures["treeArrayTex"].Resource,
                _textures["streetTex1"].Resource,
                _textures["pineTex"].Resource,
                _textures["treeTex"].Resource,
                _textures["tree2Tex"].Resource,
                _textures["tree3Tex"].Resource,
                _textures["grassTex"].Resource,
                _textures["parking"].Resource,
                _textures["parking2"].Resource,

            };
            Resource skyTex = _textures["skyCubeMap"].Resource;

            var srvDesc = new ShaderResourceViewDescription
            {
                Shader4ComponentMapping = D3DUtil.DefaultShader4ComponentMapping,
                Dimension = ShaderResourceViewDimension.Texture2D,
                Texture2D = new ShaderResourceViewDescription.Texture2DResource
                {
                    MostDetailedMip = 0,
                    ResourceMinLODClamp = 0.0f
                }
            };

            foreach (Resource tex2D in tex2DList)
            {
                srvDesc.Format = tex2D.Description.Format;
                srvDesc.Texture2D.MipLevels = tex2D.Description.MipLevels;
                Device.CreateShaderResourceView(tex2D, srvDesc, hDescriptor);

                // Next descriptor.
                hDescriptor += CbvSrvUavDescriptorSize;
            }

            srvDesc.Dimension = ShaderResourceViewDimension.TextureCube;
            srvDesc.TextureCube = new ShaderResourceViewDescription.TextureCubeResource
            {
                MostDetailedMip = 0,
                MipLevels = skyTex.Description.MipLevels,
                ResourceMinLODClamp = 0.0f
            };
            srvDesc.Format = skyTex.Description.Format;
            Device.CreateShaderResourceView(skyTex, srvDesc, hDescriptor);

            _skyTexHeapIndex = _textures.Count-1;

        }

        private void BuildShadersAndInputLayout()
        {
            _shaders["standardVS"] = D3DUtil.CompileShader("Shaders\\Default.hlsl", "VS", "vs_5_1");
            _shaders["opaquePS"] = D3DUtil.CompileShader("Shaders\\Default.hlsl", "PS", "ps_5_1");

            _shaders["skyVS"] = D3DUtil.CompileShader("Shaders\\Sky.hlsl", "VS", "vs_5_1");
            _shaders["skyPS"] = D3DUtil.CompileShader("Shaders\\Sky.hlsl", "PS", "ps_5_1");

            _inputLayout = new InputLayoutDescription(new[]
            {
                new InputElement("POSITION", 0, Format.R32G32B32_Float, 0, 0),
                new InputElement("NORMAL", 0, Format.R32G32B32_Float, 12, 0),
                new InputElement("TEXCOORD", 0, Format.R32G32_Float, 24, 0)
            });
        }

        private void BuildShapeGeometry()
        {
            //
            // We are concatenating all the geometry into one big vertex/index buffer. So
            // define the regions in the buffer each submesh covers.
            //

            var vertices = new List<Vertex>();
            var indices = new List<short>();

            SubmeshGeometry root = AppendMeshData(GeometryGenerator.CreateBox(4.0f, 0.1f, 200.0f, 3), vertices, indices);
            SubmeshGeometry root1 = AppendMeshData(GeometryGenerator.CreateBox(4.0f, 0.1f, 200.0f, 3), vertices, indices);
            SubmeshGeometry root2 = AppendMeshData(GeometryGenerator.CreateBox(4.0f, 0.1f, 90.0f, 3), vertices, indices);
            SubmeshGeometry root3 = AppendMeshData(GeometryGenerator.CreateBox(31.0f, 0.1f, 5.0f, 3), vertices, indices);
            SubmeshGeometry root5 = AppendMeshData(GeometryGenerator.CreateBox(27.7f, 0.1f, 5.0f, 3), vertices, indices);
            SubmeshGeometry root6 = AppendMeshData(GeometryGenerator.CreateBox(27.7f, 0.1f, 5.0f, 3), vertices, indices);
            SubmeshGeometry root4 = AppendMeshData(GeometryGenerator.CreateBox(10.0f, 0.1f, 8.0f, 3), vertices, indices);
            SubmeshGeometry parkingBox1 = AppendMeshData(GeometryGenerator.CreateBox(21.0f, 0.1f, 25.0f, 3), vertices, indices);
            SubmeshGeometry parkingBox = AppendMeshData(GeometryGenerator.CreateBox(18.0f, 0.1f, 18.0f, 3), vertices, indices);
            SubmeshGeometry grid = AppendMeshData(GeometryGenerator.CreateGrid(300.0f, 400.0f, 70, 40), vertices, indices);
            SubmeshGeometry sphere = AppendMeshData(GeometryGenerator.CreateSphere(0.5f, 20, 20), vertices, indices);
            SubmeshGeometry building1 = AppendMeshData(GeometryGenerator.CreateBox(5.0f, 13.0f, 5.0f, 3), vertices, indices);
            SubmeshGeometry building2 = AppendMeshData(GeometryGenerator.CreateBox(5.0f, 8.0f, 5.0f, 3), vertices, indices);
            SubmeshGeometry building3 = AppendMeshData(GeometryGenerator.CreateBox(8.0f, 4.5f, 8.0f, 3), vertices, indices);
            SubmeshGeometry grassBox = AppendMeshData(GeometryGenerator.CreateBox(1.0f, 0.1f, 1.0f, 3), vertices, indices);
            SubmeshGeometry grassBoxPin = AppendMeshData(GeometryGenerator.CreateBox(10.0f, 0.1f, 10.0f, 3), vertices, indices);
            SubmeshGeometry tree = AppendMeshData(GeometryGenerator.CreateBox(2.5f, 7.5f, 0.0f, 3), vertices, indices);
            SubmeshGeometry tree1 = AppendMeshData(GeometryGenerator.CreateBox(0.0f, 7.5f, 2.5f, 3), vertices, indices);
            SubmeshGeometry roof1 = AppendMeshData(GeometryGenerator.CreateBox(10.0f, 0.5f, 10.0f, 3), vertices, indices);
            SubmeshGeometry roof2 = AppendMeshData(GeometryGenerator.CreateBox(10.0f, 0.5f, 10.0f, 3), vertices, indices);
            SubmeshGeometry roof3 = AppendMeshData(GeometryGenerator.CreateBox(16.0f, 0.5f, 16.0f, 3), vertices, indices);
            SubmeshGeometry treePin1 = AppendMeshData(GeometryGenerator.CreateBox(7.5f, 17.5f, 0.0f, 3), vertices, indices);
            SubmeshGeometry treePin = AppendMeshData(GeometryGenerator.CreateBox(0.0f, 17.5f, 7.5f, 3), vertices, indices);

            var geo = MeshGeometry.New(Device, CommandList, vertices, indices.ToArray(), "shapeGeo");

            geo.DrawArgs["root"] = root;
            geo.DrawArgs["root1"] = root1;
            geo.DrawArgs["root2"] = root2;
            geo.DrawArgs["root3"] = root3;
            geo.DrawArgs["root4"] = root4;
            geo.DrawArgs["root5"] = root5;
            geo.DrawArgs["root6"] = root6;
            geo.DrawArgs["tree"] = tree;
            geo.DrawArgs["tree1"] = tree1;
            geo.DrawArgs["treePin"] = treePin;
            geo.DrawArgs["treePin1"] = treePin1;
            geo.DrawArgs["grid"] = grid;
            geo.DrawArgs["sphere"] = sphere;
            geo.DrawArgs["building1"] = building1;
            geo.DrawArgs["building2"] = building2;
            geo.DrawArgs["building3"] = building3;
            geo.DrawArgs["roof1"] = roof1;
            geo.DrawArgs["roof2"] = roof2;
            geo.DrawArgs["roof3"] = roof3;
            geo.DrawArgs["grassBox"] = grassBox;
            geo.DrawArgs["grassBoxPin"] = grassBoxPin;
            geo.DrawArgs["parkingBox"] = parkingBox;
            geo.DrawArgs["parkingBox1"] = parkingBox1;

            _geometries[geo.Name] = geo;
        }

        private SubmeshGeometry AppendMeshData(GeometryGenerator.MeshData meshData, List<Vertex> vertices, List<short> indices)
        {
            //
            // Define the SubmeshGeometry that cover different
            // regions of the vertex/index buffers.
            //

            var submesh = new SubmeshGeometry
            {
                IndexCount = meshData.Indices32.Count,
                StartIndexLocation = indices.Count,
                BaseVertexLocation = vertices.Count
            };

            //
            // Extract the vertex elements we are interested in and pack the
            // vertices and indices of all the meshes into one vertex/index buffer.
            //

            vertices.AddRange(meshData.Vertices.Select(vertex => new Vertex
            {
                Pos = vertex.Position,
                Normal = vertex.Normal,
                TexC = vertex.TexC
            }));
            indices.AddRange(meshData.GetIndices16());

            return submesh;
        }
        private void BuildPSOs()
        {
            //
            // PSO for opaque objects.
            //

            var opaquePsoDesc = new GraphicsPipelineStateDescription
            {
                InputLayout = _inputLayout,
                RootSignature = _rootSignature,
                VertexShader = _shaders["standardVS"],
                PixelShader = _shaders["opaquePS"],
                RasterizerState = RasterizerStateDescription.Default(),
                BlendState = BlendStateDescription.Default(),
                DepthStencilState = DepthStencilStateDescription.Default(),
                SampleMask = unchecked((int)uint.MaxValue),
                PrimitiveTopologyType = PrimitiveTopologyType.Triangle,
                RenderTargetCount = 1,
                SampleDescription = new SampleDescription(MsaaCount, MsaaQuality),
                DepthStencilFormat = DepthStencilFormat
            };
            opaquePsoDesc.RenderTargetFormats[0] = BackBufferFormat;
            _psos["opaque"] = Device.CreateGraphicsPipelineState(opaquePsoDesc);

            //
            // PSO for sky.
            //

            GraphicsPipelineStateDescription skyPsoDesc = opaquePsoDesc.Copy();
            // The camera is inside the sky sphere, so just turn off culling.
            skyPsoDesc.RasterizerState.CullMode = CullMode.None;
            // Make sure the depth function is LESS_EQUAL and not just LESS.
            // Otherwise, the normalized depth values at z = 1 (NDC) will
            // fail the depth test if the depth buffer was cleared to 1.
            skyPsoDesc.DepthStencilState.DepthComparison = Comparison.LessEqual;
            skyPsoDesc.RootSignature = _rootSignature;
            skyPsoDesc.VertexShader = _shaders["skyVS"];
            skyPsoDesc.PixelShader = _shaders["skyPS"];
            _psos["sky"] = Device.CreateGraphicsPipelineState(skyPsoDesc);

            //
            // PSO for transparent objects.
            //

            GraphicsPipelineStateDescription transparentPsoDesc = opaquePsoDesc.Copy();

            var transparencyBlendDesc = new RenderTargetBlendDescription
            {
                IsBlendEnabled = true,
                LogicOpEnable = false,
                SourceBlend = BlendOption.SourceAlpha,
                DestinationBlend = BlendOption.InverseSourceAlpha,
                BlendOperation = BlendOperation.Add,
                SourceAlphaBlend = BlendOption.One,
                DestinationAlphaBlend = BlendOption.Zero,
                AlphaBlendOperation = BlendOperation.Add,
                LogicOp = LogicOperation.Noop,
                RenderTargetWriteMask = ColorWriteMaskFlags.All
            };
            transparentPsoDesc.BlendState.RenderTarget[0] = transparencyBlendDesc;

            _psos["transparent"] = Device.CreateGraphicsPipelineState(transparentPsoDesc);

        }

        private void BuildFrameResources()
        {
            for (int i = 0; i < NumFrameResources; i++)
            {
                _frameResources.Add(new FrameResource(Device, 1, _allRitems.Count, _materials.Count));
                _fenceEvents.Add(new AutoResetEvent(false));
            }
        }

        private void BuildMaterials()
        {
            AddMaterial(new Material
            {
                Name = "sky",
                MatCBIndex = 3,
                DiffuseSrvHeapIndex = 16,
                DiffuseAlbedo = Vector4.One,
                FresnelR0 = new Vector3(0.1f),
                Roughness = 1.0f
            });
            AddMaterial(new Material
            {
                Name = "house1",
                MatCBIndex = 0,
                DiffuseSrvHeapIndex = 0,
                DiffuseAlbedo = Vector4.One,
                FresnelR0 = new Vector3(0.1f),
                Roughness = 0.3f
            });
            AddMaterial(new Material
            {
                Name = "house3",
                MatCBIndex = 1,
                DiffuseSrvHeapIndex = 1,
                DiffuseAlbedo = Vector4.One,
                FresnelR0 = new Vector3(0.1f),
                Roughness = 0.3f
            });
            AddMaterial(new Material
            {
                Name = "brick",
                MatCBIndex = 2,
                DiffuseSrvHeapIndex = 2,
                DiffuseAlbedo = new Vector4(1.0f),
                FresnelR0 = new Vector3(0.01f),
                Roughness = 1.0f
            });

            AddMaterial(new Material
            {
                Name = "roof1",
                MatCBIndex = 5,
                DiffuseSrvHeapIndex = -1,
                DiffuseAlbedo = Vector4.One,
                FresnelR0 = new Vector3(0.1f),
                Roughness = 1.0f
            });
            AddMaterial(new Material
            {
                Name = "house2",
                MatCBIndex = 6,
                DiffuseSrvHeapIndex = 3,
                DiffuseAlbedo = new Vector4(1.0f),
                FresnelR0 = new Vector3(0.01f),
                Roughness = 0.3f
            });
            AddMaterial(new Material
            {
                Name = "house4",
                MatCBIndex = 7,
                DiffuseSrvHeapIndex = 4,
                DiffuseAlbedo = new Vector4(1.0f),
                FresnelR0 = new Vector3(0.01f),
                Roughness = 0.125f
            });
            AddMaterial(new Material
            {
                Name = "house5",
                MatCBIndex = 8,
                DiffuseSrvHeapIndex = 5,
                DiffuseAlbedo = new Vector4(1.0f),
                FresnelR0 = new Vector3(0.01f),
                Roughness = 0.3f
            });
            AddMaterial(new Material
            {
                Name = "way",
                MatCBIndex = 9,
                DiffuseSrvHeapIndex = 6,
                DiffuseAlbedo = new Vector4(1.0f),
                FresnelR0 = new Vector3(0.01f),
                Roughness = 1.0f
            });
            AddMaterial(new Material
            {
                Name = "treeSprites",
                MatCBIndex = 10,
                DiffuseSrvHeapIndex = 7,
                DiffuseAlbedo = new Vector4(1.0f),
                FresnelR0 = new Vector3(0.01f),
                Roughness = 0.125f
            });
            AddMaterial(new Material
            {
                Name = "way1f",
                MatCBIndex = 11,
                DiffuseSrvHeapIndex = 8,
                DiffuseAlbedo = new Vector4(1.0f),
                FresnelR0 = new Vector3(0.01f),
                Roughness = 1.0f
            });
            AddMaterial(new Material
            {
                Name = "pine",
                MatCBIndex = 12,
                DiffuseSrvHeapIndex = 9,
                DiffuseAlbedo = new Vector4(1.0f),
                FresnelR0 = new Vector3(0.01f),
                Roughness = 0.125f
            });
            AddMaterial(new Material
            {
                Name = "tree",
                MatCBIndex = 13,
                DiffuseSrvHeapIndex = 10,
                DiffuseAlbedo = new Vector4(1.0f),
                FresnelR0 = new Vector3(0.01f),
                Roughness = 0.125f
            });
            AddMaterial(new Material
            {
                Name = "tree2",
                MatCBIndex = 13,
                DiffuseSrvHeapIndex = 11,
                DiffuseAlbedo = new Vector4(1.0f),
                FresnelR0 = new Vector3(0.01f),
                Roughness = 0.125f
            });
            AddMaterial(new Material
            {
                Name = "tree3",
                MatCBIndex = 14,
                DiffuseSrvHeapIndex = 12,
                DiffuseAlbedo = new Vector4(1.0f),
                FresnelR0 = new Vector3(0.01f),
                Roughness = 0.125f
            });
            AddMaterial(new Material
            {
                Name = "grass",
                MatCBIndex = 15,
                DiffuseSrvHeapIndex = 13,
                DiffuseAlbedo = new Vector4(1.0f),
                FresnelR0 = new Vector3(0.01f),
                Roughness = 1.0f
            });
            AddMaterial(new Material
            {
                Name = "parking",
                MatCBIndex = 16,
                DiffuseSrvHeapIndex = 14,
                DiffuseAlbedo = new Vector4(1.0f),
                FresnelR0 = new Vector3(0.01f),
                Roughness = 1.0f
            });
            AddMaterial(new Material
            {
                Name = "parking1",
                MatCBIndex = 17,
                DiffuseSrvHeapIndex = 15,
                DiffuseAlbedo = new Vector4(1.0f),
                FresnelR0 = new Vector3(0.01f),
                Roughness = 1.0f
            });
        }

        private void AddMaterial(Material mat) => _materials[mat.Name] = mat;

        private void BuildRenderItems()
        {
            int objCBIndex = 0;
            AddRenderItem(RenderLayer.Sky, objCBIndex++, "sky", "shapeGeo", "sphere",
                world: Matrix.Scaling(5000.0f));
            AddRenderItem(RenderLayer.Opaque, objCBIndex++, "brick", "shapeGeo", "grid",
                 texTransform: Matrix.Scaling(100.0f, 100.0f, 1.0f));
            AddParking(ref objCBIndex);
            AddTrees(ref objCBIndex);
            AddStreets(ref objCBIndex);
            AddBuildings(ref objCBIndex, Matrix.Scaling(1.5f, 2.0f, 1.0f));
        }

        private void TreePair(ref int objCBIndex, string material, string form1, string form2, float x, float y, float z)
        {
            AddRenderItem(RenderLayer.Transparent, objCBIndex++, material, "shapeGeo", form1,
                world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(x, y, z));
            AddRenderItem(RenderLayer.Transparent, objCBIndex++, material, "shapeGeo", form2,
                world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(x, y, z));
        }
        private void AddParking(ref int objCBIndex)
        {
            AddRenderItem(RenderLayer.Opaque, objCBIndex++, "parking", "shapeGeo", "parkingBox1",
                    world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(-25.0f, 0.0f, -160.0f));
            AddRenderItem(RenderLayer.Opaque, objCBIndex++, "parking1", "shapeGeo", "parkingBox",
                    world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(85.0f, 0.0f, 0.0f));
        }
        private void AddTrees(ref int objCBIndex)
        {
            for (int i = 0; i < 20; ++i)
            {
                if (i % 2 == 0)
                {
                    TreePair(ref objCBIndex, "pine", "treePin", "treePin1", -140.0f, 9.0f, -190.0f + i * 20.0f);
                    AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBoxPin",
                        world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(-140.0f, 0.0f, -190.0f + i * 20.0f));
                    TreePair(ref objCBIndex, "pine", "treePin", "treePin1", 140.0f, 9.0f, -190.0f + i * 20.0f);
                    AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBoxPin",
                        world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(140.0f, 0.0f, -190.0f + i * 20.0f));

                    TreePair(ref objCBIndex, "tree2", "treePin", "treePin1", -120.0f, 9.0f, -190.0f + i * 20.0f);
                    AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBoxPin",
                        world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(-120.0f, 0.0f, -190.0f + i * 20.0f));
                    TreePair(ref objCBIndex, "tree2", "treePin", "treePin1", 120.0f, 9.0f, -190.0f + i * 20.0f);
                    AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBoxPin",
                        world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(120.0f, 0.0f, -190.0f + i * 20.0f));

                    TreePair(ref objCBIndex, "tree", "treePin", "treePin1", -100.0f, 9.0f, -190.0f + i * 20.0f);
                    AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBoxPin",
                        world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(-100.0f, 0.0f, -190.0f + i * 20.0f));

                }
                else
                {
                    TreePair(ref objCBIndex, "tree2", "treePin", "treePin1", -140.0f, 9.0f, -190.0f + i * 20.0f);
                    AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBoxPin",
                        world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(-140.0f, 0.0f, -190.0f + i * 20.0f));
                    TreePair(ref objCBIndex, "tree2", "treePin", "treePin1", 140.0f, 9.0f, -190.0f + i * 20.0f);
                    AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBoxPin",
                        world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(140.0f, 0.0f, -190.0f + i * 20.0f));
                    TreePair(ref objCBIndex, "pine", "treePin", "treePin1", -120.0f, 9.0f, -190.0f + i * 20.0f);
                    AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBoxPin",
                        world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(-120.0f, 0.0f, -190.0f + i * 20.0f));
                    TreePair(ref objCBIndex, "pine", "treePin", "treePin1", 120.0f, 9.0f, -190.0f + i * 20.0f);
                    AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBoxPin",
                        world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(120.0f, 0.0f, -190.0f + i * 20.0f));
                    TreePair(ref objCBIndex, "tree2", "treePin", "treePin1", -100.0f, 9.0f, -190.0f + i * 20.0f);
                    AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBoxPin",
                        world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(-100.0f, 0.0f, -190.0f + i * 20.0f));
                }

                AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBoxPin",
                       world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(-86.0f, 0.0f, -190.0f + i * 20.0f));
                TreePair(ref objCBIndex, "tree", "tree", "tree1", -80.0f, 3.5f, -190.0f + i * 20.0f);

                

                if (i < 16)
                {
                    TreePair(ref objCBIndex, "treeSprites", "tree", "tree1", 7.0f, 3.5f, -150.0f + i * 20.0f);
                    AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBox",
                        world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(7.0f, 0.0f, -150.0f + i * 20.0f));

                    TreePair(ref objCBIndex, "tree", "tree", "tree1", 47.0f, 3.5f, -150.0f + i * 20.0f);
                    AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBox",
                        world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(47.0f, 0.0f, -150.0f + i * 20.0f));

                    TreePair(ref objCBIndex, "tree3", "tree", "tree1", 34.0f, 3.5f, -150.0f + i * 20.0f);
                    AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBox",
                        world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(34.0f, 0.0f, -150.0f + i * 20.0f));
                }

                if (i < 9)
                {
                    if (i % 2 == 0)
                    {
                        TreePair(ref objCBIndex, "tree3", "tree", "tree1", 80.0f, 3.5f, -190.0f + i * 20.0f);
                        AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBoxPin",
                            world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(80.0f, 0.0f, -190.0f + i * 20.0f));
                        TreePair(ref objCBIndex, "pine", "tree", "tree1", 100.0f, 3.5f, -190.0f + i * 20.0f);
                        AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBoxPin",
                            world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(100.0f, 0.0f, -190.0f + i * 20.0f));

                        TreePair(ref objCBIndex, "tree3", "tree", "tree1", 80.0f, 3.5f, 30.0f + i * 20.0f);
                        AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBoxPin",
                            world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(80.0f, 0.0f, 30.0f + i * 20.0f));
                        TreePair(ref objCBIndex, "pine", "tree", "tree1", 100.0f, 3.5f, 30.0f + i * 20.0f);
                        AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBoxPin",
                            world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(100.0f, 0.0f, 30.0f + i * 20.0f));
                    }
                    else
                    {
                        AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBoxPin",
                        world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(80.0f, 0.0f, -190.0f + i * 20.0f));
                        TreePair(ref objCBIndex, "tree", "tree", "tree1", 80.0f, 3.5f, -190.0f + i * 20.0f);
                        TreePair(ref objCBIndex, "treeSprites", "tree", "tree1", 100.0f, 3.5f, -190.0f + i * 20.0f);
                        AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBoxPin",
                            world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(100.0f, 0.0f, -190.0f + i * 20.0f));

                        AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBoxPin",
                        world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(80.0f, 0.0f, 30.0f + i * 20.0f));
                        TreePair(ref objCBIndex, "tree", "tree", "tree1", 80.0f, 3.5f, 30.0f + i * 20.0f);
                        TreePair(ref objCBIndex, "treeSprites", "tree", "tree1", 100.0f, 3.5f, 30.0f + i * 20.0f);
                        AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBoxPin",
                            world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(100.0f, 0.0f, 30.0f + i * 20.0f));
                    }
                }

                if (i < 10)
                {
                    TreePair(ref objCBIndex, "tree", "tree", "tree1", -47.0f, 3.5f, 0.0f + i * 20.0f);
                    AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBox",
                        world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(-47.0f, 0.0f, 0.0f + i * 20.0f));

                    TreePair(ref objCBIndex, "treeSprites", "tree", "tree1", -7.0f, 3.5f, -20.0f + i * 20.0f);
                    AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBox",
                        world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(-7.0f, 0.0f, -20.0f + i * 20.0f));

                    

                }

                if (i < 15)
                {

                    TreePair(ref objCBIndex, "tree", "tree", "tree1", -34.0f, 3.5f, -130.0f + i * 20.0f);
                    AddRenderItem(RenderLayer.Opaque, objCBIndex++, "grass", "shapeGeo", "grassBox",
                        world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(-34.0f, 0.0f, -130.0f + i * 20.0f));
                }
            }
        }
        private void AddStreets(ref int objCBIndex)
        {
            AddRenderItem(RenderLayer.Opaque, objCBIndex++, "way", "shapeGeo", "root",
               world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(0.0f, 0.0f, 0.0f));

            AddRenderItem(RenderLayer.Opaque, objCBIndex++, "way", "shapeGeo", "root1",
               world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(-70.0f, 0.0f, 0.0f));

            AddRenderItem(RenderLayer.Opaque, objCBIndex++, "way1f", "shapeGeo", "root4",
                 world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(-56.0f, 0.0f, -160.0f));

            AddRenderItem(RenderLayer.Opaque, objCBIndex++, "way", "shapeGeo", "root2",
               world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(-40.0f, 0.0f, 75.0f));

            AddRenderItem(RenderLayer.Opaque, objCBIndex++, "way1f", "shapeGeo", "root3",
                 world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(-35.0f, 0.0f, 170.0f));

            AddRenderItem(RenderLayer.Opaque, objCBIndex++, "way", "shapeGeo", "root1",
               world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(63.0f, 0.0f, 0.0f));

            AddRenderItem(RenderLayer.Opaque, objCBIndex++, "way1f", "shapeGeo", "root5",
                world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(31.5f, 0.0f, 159.0f));

            AddRenderItem(RenderLayer.Opaque, objCBIndex++, "way1f", "shapeGeo", "root6",
                world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(31.5f, 0.0f, -138.0f));

            AddRenderItem(RenderLayer.Opaque, objCBIndex++, "way1f", "shapeGeo", "root6",
               world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(31.5f, 0.0f, -165.0f));
        }
        private void AddBuildings(ref int objCBIndex, Matrix brickTexTransform)
        {
            for (int i = 0; i < 10; ++i)
            {
                AddRenderItem(RenderLayer.Opaque, objCBIndex++, "house1", "shapeGeo", "building1",
                    world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(-15.0f, 6.5f, -10.0f + i * 16.0f),
                    texTransform: brickTexTransform);
                AddRenderItem(RenderLayer.Opaque, objCBIndex++, "roof1", "shapeGeo", "roof1",
                    world: Matrix.Translation(-15.0f, 13.19f, -10.0f + i * 16.0f));

                AddRenderItem(RenderLayer.Opaque, objCBIndex++, "house1", "shapeGeo", "building1",
                    world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(+15.0f, 6.5f, -10.0f + i * 17.0f),
                    texTransform: brickTexTransform);
                AddRenderItem(RenderLayer.Opaque, objCBIndex++, "roof1", "shapeGeo", "roof1",
                    world: Matrix.Translation(+15.0f, 13.19f, -10.0f + i * 17.0f));

                AddRenderItem(RenderLayer.Opaque, objCBIndex++, "house3", "shapeGeo", "building2",
                    world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(-25.0f, 4.0f, -10.0f + i * 16.0f),
                    texTransform: brickTexTransform);
                AddRenderItem(RenderLayer.Opaque, objCBIndex++, "roof1", "shapeGeo", "roof2",
                    world: Matrix.Translation(-25.0f, 8.1f, -10.0f + i * 16.0f));

                AddRenderItem(RenderLayer.Opaque, objCBIndex++, "house3", "shapeGeo", "building2",
                    world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(-55.0f, 4.0f, -10.0f + i * 17.0f),
                    texTransform: brickTexTransform);
                AddRenderItem(RenderLayer.Opaque, objCBIndex++, "roof1", "shapeGeo", "roof2",
                    world: Matrix.Translation(-55.0f, 8.1f, -10.0f + i * 17.0f));

                AddRenderItem(RenderLayer.Opaque, objCBIndex++, "house3", "shapeGeo", "building2",
                    world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(25.0f, 4.0f, -150.0f + i * 26.0f),
                    texTransform: brickTexTransform);
                AddRenderItem(RenderLayer.Opaque, objCBIndex++, "roof1", "shapeGeo", "roof2",
                    world: Matrix.Translation(25.0f, 8.1f, -150.0f + i * 26.0f));

                AddRenderItem(RenderLayer.Opaque, objCBIndex++, "house2", "shapeGeo", "building2",
                    world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(45.0f, 4.0f, -100.0f + i * 30.0f),
                    texTransform: brickTexTransform);
                AddRenderItem(RenderLayer.Opaque, objCBIndex++, "roof1", "shapeGeo", "roof2",
                    world: Matrix.Translation(45.0f, 8.1f, -100.0f + i * 30.0f));
            }
            for (int i = 0; i < 5; ++i)
            {
                AddRenderItem(RenderLayer.Opaque, objCBIndex++, "house4", "shapeGeo", "building3",
                    world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(-15.0f, 2.3f, -120.0f + i * 20.0f),
                    texTransform: brickTexTransform);
                AddRenderItem(RenderLayer.Opaque, objCBIndex++, "roof1", "shapeGeo", "roof3",
                    world: Matrix.Translation(-15.0f, 4.7f, -120.0f + i * 20.0f));

                AddRenderItem(RenderLayer.Opaque, objCBIndex++, "house5", "shapeGeo", "building1",
                   world: Matrix.Scaling(2.0f, 1.0f, 2.0f) * Matrix.Translation(-45.0f, 6.5f, -120.0f + i * 24.0f),
                   texTransform: brickTexTransform);
                AddRenderItem(RenderLayer.Opaque, objCBIndex++, "roof1", "shapeGeo", "roof1",
                    world: Matrix.Translation(-45.0f, 13.19f, -120.0f + i * 24.0f));
            }
        }
        private void AddRenderItem(RenderLayer layer, int objCBIndex, string matName, string geoName, string submeshName,
            Matrix? world = null, Matrix? texTransform = null)
        {
            MeshGeometry geo = _geometries[geoName];
            SubmeshGeometry submesh = geo.DrawArgs[submeshName];
            var renderItem = new RenderItem
            {
                ObjCBIndex = objCBIndex,
                Mat = _materials[matName],
                Geo = geo,
                IndexCount = submesh.IndexCount,
                StartIndexLocation = submesh.StartIndexLocation,
                BaseVertexLocation = submesh.BaseVertexLocation,
                World = world ?? Matrix.Identity,
                TexTransform = texTransform ?? Matrix.Identity
            };
            _ritemLayers[layer].Add(renderItem);
            _allRitems.Add(renderItem);
        }
        private void DrawRenderItems(GraphicsCommandList cmdList, List<RenderItem> ritems)
        {
            int objCBByteSize = D3DUtil.CalcConstantBufferByteSize<ObjectConstants>();

            Resource objectCB = CurrFrameResource.ObjectCB.Resource;

            foreach (RenderItem ri in ritems)
            {
                cmdList.SetVertexBuffer(0, ri.Geo.VertexBufferView);
                cmdList.SetIndexBuffer(ri.Geo.IndexBufferView);
                cmdList.PrimitiveTopology = ri.PrimitiveType;

                long objCBAddress = objectCB.GPUVirtualAddress + ri.ObjCBIndex * objCBByteSize;

                cmdList.SetGraphicsRootConstantBufferView(0, objCBAddress);

                cmdList.DrawIndexedInstanced(ri.IndexCount, 1, ri.StartIndexLocation, ri.BaseVertexLocation, 0);
            }
        }

        // Applications usually only need a handful of samplers. So just define them all up front
        // and keep them available as part of the root signature.
        private static StaticSamplerDescription[] GetStaticSamplers() => new[]
        {
            // PointWrap
            new StaticSamplerDescription(ShaderVisibility.All, 0, 0)
            {
                Filter = Filter.MinMagMipPoint,
                AddressUVW = TextureAddressMode.Wrap
            },
            // PointClamp
            new StaticSamplerDescription(ShaderVisibility.All, 1, 0)
            {
                Filter = Filter.MinMagMipPoint,
                AddressUVW = TextureAddressMode.Clamp
            },
            // LinearWrap
            new StaticSamplerDescription(ShaderVisibility.All, 2, 0)
            {
                Filter = Filter.MinMagMipLinear,
                AddressUVW = TextureAddressMode.Wrap
            },
            // LinearClamp
            new StaticSamplerDescription(ShaderVisibility.All, 3, 0)
            {
                Filter = Filter.MinMagMipLinear,
                AddressUVW = TextureAddressMode.Clamp
            },
            // AnisotropicWrap
            new StaticSamplerDescription(ShaderVisibility.All, 4, 0)
            {
                Filter = Filter.Anisotropic,
                AddressUVW = TextureAddressMode.Wrap,
                MipLODBias = 0.0f,
                MaxAnisotropy = 8
            },
            // AnisotropicClamp
            new StaticSamplerDescription(ShaderVisibility.All, 5, 0)
            {
                Filter = Filter.Anisotropic,
                AddressUVW = TextureAddressMode.Clamp,
                MipLODBias = 0.0f,
                MaxAnisotropy = 8
            }
        };
    }
}
